#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent/SRDescent.h>
#include <atomic>

class AsiPlugin : public SRDescent {
	class GifPlayer *player = nullptr;

	size_t did, lid, rid;

	class CD3DRender *render = nullptr;

	std::atomic_bool gif_loaded = false;

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();

protected:
	void draw();
	void lost();
	void reset();
};

#endif // MAIN_H
