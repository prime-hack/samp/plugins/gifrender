#include "main.h"
#include <d3d9/DrawHook.h>
#include <d3d9/d3drender.h>
#include <GifPlayer.h>
#include <thread>

AsiPlugin::AsiPlugin() : SRDescent( nullptr ) {
	did = g_class.draw->onDraw += std::tuple{ this, &AsiPlugin::draw };
	lid = g_class.draw->onPreReset += std::tuple{ this, &AsiPlugin::lost };
	rid = g_class.draw->onPostReset += std::tuple{ this, &AsiPlugin::reset };

	std::thread( [this] {
		player	   = new GifPlayer( "anim.gif" );
		gif_loaded = true;
	} ).detach();
}

AsiPlugin::~AsiPlugin() {
	g_class.draw->onPostReset -= rid;
	g_class.draw->onPreReset -= lid;
	g_class.draw->onDraw -= did;

	delete player;

	if ( render ) g_class.draw->d3d9_releaseRender( render );
}

void AsiPlugin::draw() {
	if ( !gif_loaded ) return;

	static bool texturesInitialized = false;
	if ( !texturesInitialized ) {
		player->Initialize( DrawHook::d3d9_device() );
		texturesInitialized = true;
	}
	if ( !render ) render = g_class.draw->d3d9_createRender();

	auto texture = player->ProcessPlay();
	render->D3DBindTexture( texture );
	render->D3DTexQuad( 0, 0, player->GetWidth(), player->GetHeight() );
	render->D3DBindTexture( nullptr );
}

void AsiPlugin::lost() {
	if ( !gif_loaded ) return;
	if ( player ) player->Invalidate();
}

void AsiPlugin::reset() {
	if ( !gif_loaded ) return;
	if ( player ) player->Initialize( DrawHook::d3d9_device() );
}
